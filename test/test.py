import os
import shutil
import base64
from random import randint

import pytest
import boto3
import requests
import subprocess

from bitbucket_pipes_toolkit.test import PipeTestCase
from bitbucket_pipes_toolkit.helpers import get_variable


class TargetHostPublicIpNotFound(Exception):
    pass


PIPE_ID_NAME = 'sftp'

AWS_USER = "ec2-user"
AWS_DEFAULT_REGION = "us-east-1"
AWS_STACK_NAME = f"bbci-pipes-test-infrastructure-ec2-{PIPE_ID_NAME}-{get_variable('BITBUCKET_BUILD_NUMBER')}"
REMOTE_PATH = f"/usr/share/nginx/html/{PIPE_ID_NAME}"
LOCAL_PATH = "test/tmp"

DIRNAME = os.path.dirname(__file__)
BASE_LOCAL_DIR = os.path.join(DIRNAME, 'tmp')
LOCAL_DIR = os.path.join(BASE_LOCAL_DIR, PIPE_ID_NAME)

TEST_SSH_CONFIG_DIR = "/opt/atlassian/pipelines/agent/build/test_ssh"
BAD_SSH_CONFIG_DIR = "/opt/atlassian/pipelines/agent/build/bad_ssh"


def get_target_host_ip(stack_name):
    client = boto3.client('ec2', region_name=AWS_DEFAULT_REGION)
    instances = client.describe_instances(
        Filters=[
            {'Name': 'tag:aws:cloudformation:stack-name', 'Values': [stack_name]},
            {'Name': 'instance-state-name', 'Values': ['running']},
        ]
    )
    try:
        return instances['Reservations'][0]['Instances'][0]['PublicIpAddress']
    except IndexError:
        raise TargetHostPublicIpNotFound(stack_name)


def setup_test_ssh_dir(target_host_ip):
    os.makedirs(TEST_SSH_CONFIG_DIR, exist_ok=True)

    # Copy test infrastructure key to test directory
    test_ssh_key = os.getenv('SSH_TEST_KEY')
    with open(f"{TEST_SSH_CONFIG_DIR}/id_rsa_tmp", "w") as f:
        key_content = base64.b64decode(test_ssh_key)
        f.write(key_content.decode())

    # Add auto generated EC2 instance known_hosts entries.
    with open(f"{TEST_SSH_CONFIG_DIR}/known_hosts", 'ab+') as f:
        resp = subprocess.run([
            'ssh-keyscan',
            '-t',
            'rsa',
            target_host_ip,
        ], stdout=subprocess.PIPE)
        f.write(resp.stdout)


def setup_file(dirname, msg):
    """
    Setup files to sync with remote server
    """
    result_files = []

    random_number = randint(0, 100)

    # clean up & create paths
    rm_files(BASE_LOCAL_DIR)

    try:
        os.makedirs(os.path.join(dirname, 'subdir'))
    except OSError:
        print(f'Creation of the directory {dirname} failed')
    else:
        print(f'Successfully created the directory {dirname}')

    for i in range(1, 3+1):
        nested_dir = 'subdir/' if i % 3 == 0 else ''
        path = f'{dirname}/{nested_dir}deployment-{random_number}-{i}.txt'
        result_files.append(f'{nested_dir}deployment-{random_number}-{i}.txt')

        with open(path, 'w') as f:
            f.write(f'Pipelines is awesome {random_number}! {msg}')

    return result_files


def rm_files(path):
    """
    Clean up test generated files
    """
    try:
        shutil.rmtree(path, ignore_errors=True)
    except OSError:
        print(f'Deletion of the directory {path} failed')
    else:
        print(f'Successfully deleted the directory {path}')


class SftpDeployTestCase(PipeTestCase):
    ssh_config_dir = '/opt/atlassian/pipelines/agent/ssh'
    ssh_key_file = 'id_rsa_tmp'

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        # get Variables
        cls.USER = AWS_USER
        cls.SERVER_IP = get_target_host_ip(AWS_STACK_NAME)
        cls.LOCAL_PATH = LOCAL_PATH
        cls.REMOTE_PATH = REMOTE_PATH
        cls.BASE_URL = f"http://{cls.SERVER_IP}/{PIPE_ID_NAME}/tmp/{PIPE_ID_NAME}"

        setup_test_ssh_dir(cls.SERVER_IP)

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.api_client = self.docker_client.api

    def tearDown(self):
        # clean test generated files
        rm_files(BASE_LOCAL_DIR)

    def test_fail_no_parameters(self):
        result = self.run_container()
        self.assertIn('SERVER variable missing', result)

    def test_default_success(self):
        # generate new files
        message = 'default_success'
        generated_files = setup_file(LOCAL_DIR, message)

        result = self.run_container(
            environment={
                'USER': self.USER,
                'SERVER': self.SERVER_IP,
                'REMOTE_PATH': self.REMOTE_PATH,
                'LOCAL_PATH': self.LOCAL_PATH,
            },
            volumes={
                TEST_SSH_CONFIG_DIR: {'bind': self.ssh_config_dir, 'mode': 'rw'}
            })

        self.assertIn(f'Deployment finished', result)

        generated_number = generated_files[0].split('-')[1]

        for generated_file in generated_files:
            resp = requests.get(f"{self.BASE_URL}/{generated_file}")

            self.assertIn(
                f'Pipelines is awesome {generated_number}! {message}',
                resp.text
            )

    def test_success_password(self):
        # generate new files
        message = 'default_success'
        generated_files = setup_file(LOCAL_DIR, message)

        result = self.run_container(
            environment={
                'USER': self.USER,
                'SERVER': self.SERVER_IP,
                'REMOTE_PATH': self.REMOTE_PATH,
                'LOCAL_PATH': self.LOCAL_PATH,
                'PASSWORD': get_variable('PASSWORD'),
            },
            volumes={
                TEST_SSH_CONFIG_DIR: {'bind': self.ssh_config_dir, 'mode': 'rw'}
            })

        self.assertIn(f'Deployment finished', result)

        generated_number = generated_files[0].split('-')[1]

        for generated_file in generated_files:
            resp = requests.get(f"{self.BASE_URL}/{generated_file}")

            self.assertIn(
                f'Pipelines is awesome {generated_number}! {message}',
                resp.text
            )

    def test_success_arg_ssh_key(self):
        # generate new files
        message = 'success_arg_ssh_key'
        generated_files = setup_file(LOCAL_DIR, message)

        with open(os.path.join(TEST_SSH_CONFIG_DIR, self.ssh_key_file),
                  'rb') as identity_file:
            identity_content = identity_file.read()

        result = self.run_container(
            environment={
                'USER': self.USER,
                'SERVER': self.SERVER_IP,
                'REMOTE_PATH': self.REMOTE_PATH,
                'LOCAL_PATH': self.LOCAL_PATH,
                'SSH_KEY': base64.b64encode(identity_content),
            },
            volumes={
                TEST_SSH_CONFIG_DIR: {'bind': self.ssh_config_dir, 'mode': 'rw'}
            })

        self.assertIn(f'Deployment finished', result)

        generated_number = generated_files[0].split('-')[1]

        for generated_file in generated_files:
            resp = requests.get(f"{self.BASE_URL}/{generated_file}")

            self.assertIn(
                f'Pipelines is awesome {generated_number}! {message}',
                resp.text
            )

    def test_fail_bad_user(self):
        result = self.run_container(
            environment={
                'USER': 'ec2-user-will-not-work',
                'SERVER': self.SERVER_IP,
                'REMOTE_PATH': self.REMOTE_PATH,
                'LOCAL_PATH': self.LOCAL_PATH,
            },
            volumes={
                TEST_SSH_CONFIG_DIR: {'bind': self.ssh_config_dir, 'mode': 'rw'}
            })

        self.assertIn(f'Permission denied', result)

    def test_fail_bad_known_host(self):
        # create bad ssh
        shutil.copytree(TEST_SSH_CONFIG_DIR, BAD_SSH_CONFIG_DIR)
        with open(f"{BAD_SSH_CONFIG_DIR}/known_hosts", 'w') as f:
            f.write('')

        result = self.run_container(
            environment={
                'USER': self.USER,
                'SERVER': self.SERVER_IP,
                'REMOTE_PATH': self.REMOTE_PATH,
                'LOCAL_PATH': self.LOCAL_PATH,
            },
            volumes={
                BAD_SSH_CONFIG_DIR: {'bind': self.ssh_config_dir, 'mode': 'rw'}
            })

        self.assertIn(f'Host key verification failed', result)
