FROM alpine:3.9

RUN apk --no-cache add \
    openssh=7.9_p1-r6 \
    sshpass=1.06-r0 \
    bash=4.4.19-r1 && \
    wget --no-verbose -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh

COPY pipe.sh /
COPY LICENSE.txt README.md pipe.yml /

ENTRYPOINT ["/pipe.sh"]
